﻿using DAL;
using DAL.Models;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using IMS.Models.DTO.Patch;
using IMS.Models.DTO.Request;
using IMS.Models.DTO.Response;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IMS.Controllers.API
{
    public class ItemController : ApiController
    {
        //Constructors

        public ItemController()
        {
            mItemRepository = new EntityRepository<Item>( new DBContext() );
        }

        //Methods

        [HttpGet]
        [ResponseType(typeof(IItem))]
        public async Task<IHttpActionResult> GetItem( int id )
        {
            var item = await mItemRepository
                .GetSingleOrDefaultWhere( queriedItem => queriedItem.ID == id );

            if (item != null)
            {
                var response = new ItemResponse();

                response.ParseExisting(item);

                return Ok(response);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<IItem>))]
        public async Task<IHttpActionResult> GetItemList( int page = 1, int take = 10 )
        {
            var itemList = await mItemRepository
                .Paginate( page, take, order => order.ID );

            return Ok( itemList.ToPaginatedDto<ItemResponse, Item>() );
        }

        [HttpPost]
        [ResponseType(typeof(IItem))]
        public async Task<IHttpActionResult> CreateItem( [FromBody] ItemRequest itemRequest )
        {
            var item = itemRequest.ToDB();

            mItemRepository.Add(item);
            await mItemRepository.Save();

            return CreatedAtRoute("DefaultApi",
                new { ID = item.ID }, 
                item);
        }

        [HttpPatch]
        public async Task<IHttpActionResult> UpdateItem( int id, [FromBody]ItemPatchRequest itemRequest )
        {
            var item = await mItemRepository
                .GetSingleOrDefaultWhere( queriedItem => queriedItem.ID == id );

            if (item != null)
            {
                item = itemRequest.ToDB(item);

                mItemRepository.Edit(item);
                await mItemRepository.Save();

                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteItem( int id )
        {
            var item = await mItemRepository
                .GetSingleOrDefaultWhere(queriedItem => queriedItem.ID == id);

            if (item != null)
            {
                mItemRepository.Delete(item);

                await mItemRepository.Save();

                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                return StatusCode(System.Net.HttpStatusCode.NotFound);
            }
        }

        //Fields

        private EntityRepository<Item> mItemRepository;
    }
}