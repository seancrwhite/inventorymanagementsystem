﻿using DAL;
using DAL.Models;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using IMS.Models.DTO.Patch;
using IMS.Models.DTO.Request;
using IMS.Models.DTO.Response;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IMS.Controllers.API
{
    public class LocationController : ApiController
    {
        #region Constructors

        public LocationController()
        {
            mLocationRepository = new EntityRepository<Location>(new DBContext());
            mItemRepository = new EntityRepository<Item>(new DBContext());
        }

        #endregion Constructors

        #region Methods

        [HttpGet]
        [ResponseType(typeof(ILocation))]
        public async Task<IHttpActionResult> GetLocation(int id)
        {
            var location = await mLocationRepository
                .GetSingleOrDefaultWhere(queriedLocation => queriedLocation.ID == id);

            if (location != null)
            {
                var response = new LocationResponse();

                response.ParseExisting(location);

                return Ok(response);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<ILocation>))]
        public async Task<IHttpActionResult> GetLocationList(int page = 1, int take = 10)
        {
            var itemList = await mLocationRepository
                .Paginate(page, take, order => order.ID);

            return Ok(itemList.ToPaginatedDto<LocationResponse, Location>());
        }

        [HttpPost]
        [ResponseType(typeof(ILocation))]
        public async Task<IHttpActionResult> CreateLocation([FromBody] LocationRequest locationRequest)
        {
            var location = locationRequest.ToDB();

            mLocationRepository.Add(location);
            await mLocationRepository.Save();

            return CreatedAtRoute("DefaultApi",
                new { Id = location.ID },
                location);
        }

        [HttpPatch]
        public async Task<IHttpActionResult> UpdateLocation(int id, [FromBody]LocationPatchRequest locationRequest)
        {
            var location = await mLocationRepository
                .GetSingleOrDefaultWhere(queriedLocation => queriedLocation.ID == id);

            if (location != null)
            {
                location = locationRequest.ToDB(location);

                mLocationRepository.Edit(location);
                await mLocationRepository.Save();

                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteLocations(int id)
        {
            var location = await mLocationRepository
                .GetSingleOrDefaultWhere(queriedLocation => queriedLocation.ID == id);

            if (location != null)
            {
                var associatedItems = await mItemRepository
                    .GetAllWhere(item => item.LocationID == id);

                foreach (var item in associatedItems)
                {
                    item.LocationID = null;

                    mItemRepository.Edit(item);
                }
                
                await mItemRepository.Save();

                mLocationRepository.Delete(location);

                await mLocationRepository.Save();

                return StatusCode(System.Net.HttpStatusCode.NoContent);
            }
            else
            {
                return StatusCode(System.Net.HttpStatusCode.NotFound);
            }
        }

        #endregion Methods

        #region Properties

        private EntityRepository<Item> mItemRepository;
        private EntityRepository<Location> mLocationRepository;

        #endregion Properties
    }
}