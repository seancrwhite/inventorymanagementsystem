﻿using System;

namespace IMS.Models.DataContracts
{
    interface IItem
    {
        int ID { get; }

        int? LocationID { get; }

        int Quantity { get; }

        string Name { get; }

        string Description { get; }

        DateTime? Expires { get; }
    }
}
