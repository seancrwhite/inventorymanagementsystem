﻿namespace IMS.Models.DataContracts
{
    interface ILocation
    {
        int ID { get; }

        string Name { get; }

        string Description { get; }
    }
}
