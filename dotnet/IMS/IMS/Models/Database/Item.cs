﻿using DAL.Models.DB;
using IMS.Models.DataContracts;
using System;
using System.ComponentModel.DataAnnotations;

namespace IMS.Models.Database
{
    public class Item : IEntity, IItem
    {
        public Item()
        {
            LastUpdated = Created = DateTime.Now;
        }

        [Key]
        public int ID { get; set; }

        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }

        public int Quantity { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? Expires{ get; set; }

        public DateTime Created { get; set; }

        public DateTime? LastUpdated { get; set; }
    }
}