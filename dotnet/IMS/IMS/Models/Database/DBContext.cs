﻿using IMS.Migrations;
using System.Data.Entity;

namespace IMS.Models.Database
{
    public class DBContext : DbContext
    {
        public DBContext() : base("DefaultConnection")
        {
            System.Data.Entity.Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<DBContext, Configuration>() );
        }

        public virtual DbSet<Location> Location { get; set; }

        public virtual DbSet<Item> Item { get; set; }
    }
}