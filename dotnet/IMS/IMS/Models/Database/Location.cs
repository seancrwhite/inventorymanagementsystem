﻿using DAL.Models.DB;
using IMS.Models.DataContracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMS.Models.Database
{
    public class Location : IEntity, ILocation
    {
        public Location()
        {
            LastUpdated = Created = DateTime.Now;
        }

        [Key]
        public int ID { get; set; }

        public virtual HashSet<Item> Items { get; set; } = new HashSet<Item>();

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Created { get; set; }

        public DateTime? LastUpdated { get; set; }
    }
}