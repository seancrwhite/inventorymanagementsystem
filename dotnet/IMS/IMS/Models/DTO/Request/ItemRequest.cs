﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using System;

namespace IMS.Models.DTO.Request
{
    public class ItemRequest : IItem, IRequest<Item>
    {
        // Methods

        public Item ToDB()
        {
            return new Item
            {
                ID = ID,
                Quantity = Quantity,
                Name = Name,
                Description = Description,
                Expires = Expires,
                LocationID = LocationID
            };
        }

        //Properties

        public int ID { get; set; }

        public int Quantity { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? Expires { get; set; }

        public int? LocationID { get; set; }
    }
}