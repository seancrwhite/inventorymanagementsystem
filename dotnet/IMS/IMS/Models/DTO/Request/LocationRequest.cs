﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using System;

namespace IMS.Models.DTO.Request
{
    public class LocationRequest : ILocation, IRequest<Location>
    {
        //Methods

        public Location ToDB()
        {
            return new Location
            {
                Description = Description,
                ID = ID,
                Name = Name
            };
        }

        //Properties

        public string Description { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }
    }
}