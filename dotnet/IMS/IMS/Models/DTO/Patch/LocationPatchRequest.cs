﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;

namespace IMS.Models.DTO.Patch
{
    public class LocationPatchRequest : ILocation, IPatchRequest<Location>
    {
        //Methods

        public Location ToDB(Location existing)
        {
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;

            return existing;
        }

        //Properties

        public string Description { get; set; }

        public int ID { get; private set; }

        public string Name { get; set; }
    }
}