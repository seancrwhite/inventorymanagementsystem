﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using System;

namespace IMS.Models.DTO.Patch
{
    public class ItemPatchRequest : IItem, IPatchRequest<Item>
    {
        //Methods

        public Item ToDB(Item existing)
        {
            existing.Quantity = Quantity != 0 ? Quantity : existing.Quantity;
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;
            existing.Expires = Expires ?? existing.Expires;
            existing.LocationID = LocationID ?? existing.LocationID;

            return existing;
        }

        //Properties

        public int ID { get; private set; }

        public int Quantity { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? Expires { get; set; }

        public int? LocationID { get; set; }
    }
}