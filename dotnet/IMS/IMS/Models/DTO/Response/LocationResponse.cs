﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using System;

namespace IMS.Models.DTO.Response
{
    public class LocationResponse : ILocation, IResponse<Location>
    {
        //Methods

        public void ParseExisting(Location existing)
        {
            ID = existing.ID;
            Name = existing.Name;
            Description = existing.Description;
        }

        //Properties

        public int ID { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }
    }
}