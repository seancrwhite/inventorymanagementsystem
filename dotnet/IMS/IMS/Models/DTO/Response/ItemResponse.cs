﻿using DAL.Models.DTO;
using IMS.Models.Database;
using IMS.Models.DataContracts;
using System;

namespace IMS.Models.DTO.Response
{
    public class ItemResponse : IItem, IResponse<Item>
    {
        //Methods

        public void ParseExisting(Item existing)
        {
            ID = existing.ID;
            Quantity = existing.Quantity;
            Name = existing.Name;
            Description = existing.Description;
            Expires = existing.Expires;
            LocationID = existing.LocationID;
            Created = existing.Created;
            Updated = existing.LastUpdated;
        }

        //Properties

        public int ID { get; private set; }

        public int Quantity { get; private set; }

        public string Name { get; private set; }

        public string Description { get; private set; }

        public DateTime? Expires { get; private set; }

        public int? LocationID { get; private set; }

        public DateTime Created { get; private set; }

        public DateTime? Updated { get; private set; }
    }
}