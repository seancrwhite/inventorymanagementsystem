﻿angular.module('imsMain', ['ims.Services'])
.controller('MainController', ['$scope', 'ims.Services.LocationService', 'ims.Services.ItemService',
function ($scope, LocationService, ItemService) {
    $scope.items = ItemService.query();
    $scope.locations = LocationService.query();

    $scope.item = {};
    $scope.location = {};
    $scope.sort = 'Quantity';
    $scope.locationSort = '';

    $scope.locationNames = {};
    $scope.locationNames["21"] = "Pantry";
    $scope.locationNames["22"] = "Fridge";
    $scope.locationNames["23"] = "Outside";

    $scope.deleteItem = function (item) {
        ItemService.remove({ itemId: item.ID });
        $scope.items.Items.splice($scope.items.Items.indexOf(item), 1);
    };

    $scope.addItem = function () {
        if ($scope.item.ID != null && $scope.item.ID > 0) {
            ItemService.update({ itemId: $scope.item.ID }, $scope.item);
        } else {
            ItemService.save($scope.item);
        }
    };

    $scope.updateItemLocationId = function (locationId) {
        $scope.item.locationId = locationId;
    };

    $scope.setCurrentItem = function (itemId) {
        if (itemId > 0) {
            $scope.item = ItemService.get({ itemId: itemId });
        }
    };

    $scope.sortChanged = function (sort) {
        $scope.sort = sort;
    };

    $scope.locationSortChanged = function (sort) {
        $scope.locationSort = sort;
        console.log(sort);
    };
}]);