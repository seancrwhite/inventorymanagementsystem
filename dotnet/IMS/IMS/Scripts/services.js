﻿(function () {
    angular
        .module("ims.Services", ['ngResource'])
        .factory("ims.Services.LocationService", ["$resource",
            function ($resource) {
                return $resource(
                    "/api/location/:locationId",
                    { locationId: '@locationId' },
                    {
                        query: {
                            method: "GET",
                            isArray: false
                        },
                        get: {
                            method: "GET"
                        },
                        save: {
                            method: "POST"
                        },
                        remove: {
                            method: "DELETE"
                        }
                    }
                    );
            }
        ])
        .factory("ims.Services.ItemService", ["$resource",
            function ($resource) {
                return $resource(
                    "/api/item/:itemId",
                    { itemId: '@itemId' },
                    {
                        query: {
                            method: "GET",
                            isArray: false
                        },
                        get: {
                            method: "GET"
                        },
                        save: {
                            method: "POST"
                        },
                        remove: {
                            method: "DELETE"
                        },
                        update: {
                            method: "PATCH"
                        }
                    }
                    );
            }
        ])
})();