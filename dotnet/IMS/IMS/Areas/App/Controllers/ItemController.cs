﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IMS.Areas.App.Controllers
{
    public class ItemController : Controller
    {
        // GET: App/Item
        public ActionResult Index(int id = 0)
        {
            ViewBag.id = id;
            return View();
        }
    }
}
