namespace IMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makeLocationIdNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Items", "LocationId", "dbo.Locations");
            DropIndex("dbo.Items", new[] { "LocationId" });
            AlterColumn("dbo.Items", "LocationId", c => c.Int());
            CreateIndex("dbo.Items", "LocationId");
            AddForeignKey("dbo.Items", "LocationId", "dbo.Locations", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Items", "LocationId", "dbo.Locations");
            DropIndex("dbo.Items", new[] { "LocationId" });
            AlterColumn("dbo.Items", "LocationId", c => c.Int(nullable: false));
            CreateIndex("dbo.Items", "LocationId");
            AddForeignKey("dbo.Items", "LocationId", "dbo.Locations", "Id", cascadeDelete: true);
        }
    }
}
