namespace IMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeExpirationsNullableAgain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "Expires", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Expires", c => c.DateTime(nullable: false));
        }
    }
}
