// <auto-generated />
namespace IMS.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class changeNamesOfItemVars : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(changeNamesOfItemVars));
        
        string IMigrationMetadata.Id
        {
            get { return "201702250013417_changeNamesOfItemVars"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
