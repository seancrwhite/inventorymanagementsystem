namespace IMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeNamesOfItemVars : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Items", new[] { "LocationId" });
            AddColumn("dbo.Items", "Expires", c => c.DateTime());
            CreateIndex("dbo.Items", "LocationID");
            DropColumn("dbo.Items", "ExpirationDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Items", "ExpirationDate", c => c.DateTime());
            DropIndex("dbo.Items", new[] { "LocationID" });
            DropColumn("dbo.Items", "Expires");
            CreateIndex("dbo.Items", "LocationId");
        }
    }
}
