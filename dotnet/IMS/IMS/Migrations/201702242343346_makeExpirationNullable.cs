namespace IMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makeExpirationNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "ExpirationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "ExpirationDate", c => c.DateTime(nullable: false));
        }
    }
}
